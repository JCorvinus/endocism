﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity;

namespace VRKeyboard
{
	public class HandModelReferences : MonoBehaviour
	{
		[SerializeField] HandModel leftHand;
		[SerializeField] HandModel rightHand;

		public HandModel LeftHand { get { return leftHand; } }
		public HandModel RightHand { get { return rightHand; } }
	}
}