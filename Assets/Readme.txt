Feature requests:
	General:
		Robust re-targeting of all value changes over time.
		Filter value could match to trigger pull if using controller with variable trigger?
	
Real time animation via motion controller:
How do we map a transform from controller onto a transform on an object?
	Do we use the palm position? Palm orientation?
	Specific finger positions? That would be weird for doing orientation
	Averaged everything position/orientation?
	
	Position and orientation separately? One hand for one and another hand for the other?
		(This might not work too well though because you probably want one hand to stop/start recording, the other to do the actual performance.)
		
Three modes:
	Whole Object mode: do a direct transform mapping from hand palm to target object, with optional offsets.
		position only
		orientation only
		
	Single parameter mode:
		specify a top/bottom value and move a single finger up and down.
		
	Special modes:
		- select rotation axis, then start playback. Spin using circle gesture to rotate?
		- scale/rotate mode: use double pinches to do scale/rotation?
			(when not in playback we can use these as camera zoom/rotation controls)
			
Non-modal workflow:
- Specify a parameter, and number of dimensions (If the parameter's dimensionality exceeds 1, or if there's an easy way of mapping higher dimensions to a lower one (say distance from origin))
- Specify the range mapping (a preview box/sphere of the valid space can be drawn while doing this. We can provide nice defaults by looking at the bounds of the objects near/related to the object being animated)
	There should be 2 presets, one for small scale, and one for real-scale.
* Scrub to the desired time, if necessary
* Set the desired time-scale
- Start timed recording.
	* If the recording type demands two hands, we can do a countdown timer.


You could do freeze-frame and real-time recording under this single model by setting the recording timescale to zero. Just scrub to where you want and record. The record function could auto-detect this and stop the recording after capture. This would also avoid the problem of having two different kinds of capture objects.

More mode problems, two kinds of recorders: Direct-capture and mapped-capture. Direct capture has simpler interface: Start and stop. Mapped capture will be used more often but has a bigger setup. Direct capture is best for straight up motion capture, not abstract motion capture.

This distinction isn't meaningful in any sense other than the number of inputs being mapped to outputs, and the relationship between mapping. Direct-capture means a skeletal tree structure input is being mapped directly onto a skeletal structure output. It also preserves scale and orientation perfectly. (So it might be ok to actually have them be separate modes). Perhaps not though, what if we were to only have one capture mode, but you can map hierarchies to eachother - if using leap tracking, an entire finger could be a tail for example.

---------------------------------------------------------------

Recorder interface:

Sample()
Save()

Timeline Interface:
Load()
Start()
Stop()
SetTime()