﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity;
using Leap.Unity.Examples;
using Leap.Unity.Interaction;

namespace JCV_Animation
{
	public class ExtendEffect : MonoBehaviour
	{
		Transform viewCamera;

		ProjectionPostProcessProvider projectionProvider;
		HandModelBase handModel;
		[SerializeField] InteractionHand interactionProjectionHand;
		[SerializeField] InteractionHand interactionHand;

		[Range(0, 1)]
		[SerializeField] float handNearDistance;

		[SerializeField] Color innerColor;
		[SerializeField] Color rimColor;

		Renderer meshRenderer;
		int innerColorHash;
		int rimColorHash;

		bool doFade = true;
		float fadeTime = 0;
		float fadeDuration = 0.125f;

		private void Awake()
		{
			innerColorHash = Shader.PropertyToID("_InnerColor");
			rimColorHash = Shader.PropertyToID("_RimColor");

			handModel = GetComponent<HandModelBase>();

			viewCamera = Camera.main.transform;
			projectionProvider = GetComponentInParent<ProjectionPostProcessProvider>();
			meshRenderer = GetComponentInChildren<Renderer>();
		}

		// Use this for initialization
		void Start()
		{
			fadeTime = fadeDuration;
		}

		// Update is called once per frame
		void Update()
		{
			var headPos = viewCamera.position;
			var shoulderBasis = Quaternion.LookRotation(
			  Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up),
			  Vector3.up);

			Leap.Hand hand = handModel.GetLeapHand();

			if (hand != null && handModel.IsTracked)
			{
				// Approximate shoulder position with magic values.
				var shoulderPos = headPos
									+ (shoulderBasis * (new Vector3(0f, -0.2f, -0.1f)
									+ Vector3.left * 0.1f * (hand.IsLeft ? 1f : -1f)));

				// Calculate the projection of the hand if it extends beyond the
				// handMergeDistance.
				var shoulderToHand = hand.PalmPosition.ToVector3() - shoulderPos;
				var handShoulderDist = shoulderToHand.magnitude;
				var projectionDistance = Mathf.Max(0f, handShoulderDist - projectionProvider.handMergeDistance);
				/*var projectionAmount = Mathf.Pow(1 + projectionDistance, projectionProvider.projectionExponent);
				hand.SetTransform(shoulderPos + shoulderToHand * projectionAmount,
									hand.Rotation.ToQuaternion());*/

				bool projectionHandClose = projectionDistance < handNearDistance;
				bool projectionHandUngrabbed = !interactionProjectionHand.isGraspingObject;

				bool isNormalHandBusy = interactionHand.isGraspingObject || interactionHand.isPrimaryHovering;

				doFade = (projectionHandClose && projectionHandUngrabbed) || isNormalHandBusy;

				// don't allow double-grasp
				if (interactionHand.isGraspingObject && interactionProjectionHand.isGraspingObject) interactionHand.ReleaseGrasp();

				// merge the hands if necessary.
				if(interactionProjectionHand.isGraspingObject && Mathf.Approximately(projectionDistance, 0))
				{
					IInteractionBehaviour graspedObject = interactionProjectionHand.graspedObject;
					interactionProjectionHand.ReleaseGrasp();
					interactionHand.TryGrasp(graspedObject);
				}
			}
			else
			{
				doFade = true;
				fadeTime = fadeDuration;
			}

			bool canGrasp = !doFade;
			if (interactionProjectionHand.graspingEnabled != canGrasp) interactionProjectionHand.graspingEnabled = canGrasp;

			fadeTime += (doFade) ? Time.deltaTime : -Time.deltaTime;
			fadeTime = Mathf.Clamp(fadeTime, 0, fadeDuration);
			float fadeTValue = Mathf.InverseLerp(0, fadeDuration, fadeTime);

			meshRenderer.material.SetColor(innerColorHash, Color.Lerp(innerColor, Color.clear, fadeTValue));
			meshRenderer.material.SetColor(rimColorHash, Color.Lerp(rimColor, Color.clear, fadeTValue));
		}
	}
}