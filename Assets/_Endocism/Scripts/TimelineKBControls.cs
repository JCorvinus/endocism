﻿using UnityEngine;
using System.Collections;

namespace JCV_Animation
{
    public class TimelineKBControls : MonoBehaviour
    {
        [SerializeField] Timeline timeline;

        #region UI Values
        [SerializeField] KeyCode playKey = KeyCode.W;
        [SerializeField] KeyCode timeLeftKey = KeyCode.A;
        [SerializeField] KeyCode timeRightKey = KeyCode.D;
        [SerializeField] float timeIncrement = 0.1f;
        #endregion

        void Awake()
        {
            timeline = GetComponent<Timeline>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(playKey))
            {
                if (timeline.IsPlaying) timeline.Stop();
                else timeline.Play();
            }
            else if (Input.GetKeyDown(timeLeftKey))
            {
                timeline.SetTimeIndex(timeline.CurrentTime - timeIncrement);
            }
            else if (Input.GetKeyDown(timeRightKey))
            {
                timeline.SetTimeIndex(timeline.CurrentTime + timeIncrement);
            }
        }
    }
}