﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
    public class RecordingColorChange : MonoBehaviour
    {
        [SerializeField] SkeletalPoseRecorder poseRecorder;
        [SerializeField] MeshRenderer meshRenderer;
        [SerializeField] string colorName = "_Color";
        int colorID;

        // Use this for initialization
        void Start()
        {
            colorID = Shader.PropertyToID(colorName);
        }

        // Update is called once per frame
        void Update()
        {
            if (poseRecorder != null)
            {
                meshRenderer.material.SetColor(colorID, (poseRecorder.IsRecording) ? Color.red : Color.grey);
            }
        }
    }
}