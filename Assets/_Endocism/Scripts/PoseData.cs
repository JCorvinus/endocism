﻿using UnityEngine;
using System.Collections;
using System;

namespace JCV_Animation
{
    public class PoseData : ScriptableObject, IComparer
    {
        [SerializeField] Vector3 localPosition;
        [SerializeField] Quaternion localRotation;
        [SerializeField] Vector3 localScale;

        [SerializeField] float timestamp;

        public Vector3 LocalPosition
        {
            get { return localPosition; }
            set { localPosition = value; }
        }

        public Quaternion LocalRotation
        {
            get { return localRotation; }
            set { localRotation = value; }
        }

        public Vector3 LocalScale
        {
            get { return localScale; }
            set { localScale = value; }
        }

        public float TimeStamp { get { return timestamp; } set { timestamp = value; } }

        public int Compare(object x, object y)
        {
            PoseData poseX = (PoseData)x;
            PoseData poseY = (PoseData)y;

            return (int)(poseX.timestamp - poseY.timestamp);
        }
    }
}