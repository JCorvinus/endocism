﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
	public class EnableVR : MonoBehaviour
	{
		IEnumerator Start()
		{
			UnityEngine.XR.XRSettings.LoadDeviceByName("OpenVR");

			yield return null;

			UnityEngine.XR.XRSettings.enabled = true;
			UnityEngine.XR.XRSettings.showDeviceView = true;
		}
	}
}