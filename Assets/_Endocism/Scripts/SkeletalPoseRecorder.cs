﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace JCV_Animation
{
    /// <summary>
    /// This class allows for performance capture 
    /// </summary>
    public class SkeletalPoseRecorder : MonoBehaviour
    {
        public delegate void CountdownEvent(SkeletalPoseRecorder sender, int timeRemaining);
        public event CountdownEvent CountdownTick;
        public event CountdownEvent CountdownStart;
        public event CountdownEvent CountdownFinished;

        Quaternion zeroRotation = new Quaternion()
        {
            x = 0,
            y = 0,
            z = 0,
            w = 0
        };

        [SerializeField] Transform root;
        [SerializeField] Timeline timeline;
        [SerializeField] Animation captureAnimation;

        public Animation CaptureAnimation { get { return captureAnimation; } }

        #region Editor Values
        [SerializeField] string savePath;
        [SerializeField] string fileName;

		public string SavePath { get { return savePath; } }

        [SerializeField] bool pauseOnPoseCapture;
        [SerializeField] bool reportRecordTime;
        [SerializeField] bool reportExportTime;

        [SerializeField] bool isRecording = false;
        [SerializeField] float keyframeInterval = 0.15f;

        float timeSinceLastRecord = 0;
        Coroutine recordCoroutine;

        public bool IsRecording { get { return isRecording; } }
        #endregion

        // diagnostic stuff
        System.Diagnostics.Stopwatch recordStopwatch;
        System.Diagnostics.Stopwatch exportStopwatch;

        [SerializeField] int drawDebugKeyframeIndex = 0;
        [SerializeField] bool drawDebugBones;
        [SerializeField] bool drawDebugKeyframe = false;

        // Use this for initialization
        void Start()
        {
            recordStopwatch = new System.Diagnostics.Stopwatch();
            exportStopwatch = new System.Diagnostics.Stopwatch();

            captureAnimation = ScriptableObject.CreateInstance<Animation>();
            if(root != null) BuildTree(captureAnimation, root);
            else Debug.LogError("No root transform, can't capture animation.");

            timeline.TimeDurationChanged += timeline.TimeDurationChanged;
        }

        public void OnValidate()
        {
            if (captureAnimation != null && captureAnimation.RootNode != null &&
                captureAnimation.RootNode.Keyframes != null)
            {
                drawDebugKeyframeIndex = Mathf.Clamp(drawDebugKeyframeIndex, 0, captureAnimation.RootNode.Keyframes.Length);
            }
            else drawDebugKeyframeIndex = 0;
        }

        #region Data Capture Methods
        void RecordData()
        {
            if (reportRecordTime)
            {
                recordStopwatch.Reset();
                recordStopwatch.Start();
            }

            PoseCaptureTraversal(captureAnimation.RootNode, timeline.CurrentTime);

            if (reportRecordTime)
            {
                recordStopwatch.Stop();
                Debug.Log("Recorded pose in " + recordStopwatch.ElapsedMilliseconds + " ms.");
            }
        }

        void PoseCaptureTraversal(AnimatedtransformNode transformNode, float timeIndex)
        {
            // we'll need to insert our keyframe 
            PoseData pose = RecordPose(transformNode.TransformRef, timeIndex);
            transformNode.AddKeyframeRealTime(pose);

            for(int i=0; i < transformNode.Children.Length; i++)
            {
                PoseCaptureTraversal(transformNode.Children[i], timeIndex);
            }
        }

        PoseData RecordPose(Transform currentTransform, float time)
        {
            PoseData newData = ScriptableObject.CreateInstance<PoseData>();
            newData.LocalPosition = currentTransform.localPosition;
            newData.LocalRotation = currentTransform.localRotation;
            newData.LocalScale = currentTransform.localScale;
            newData.TimeStamp = time;

            return newData;
        }
        #endregion

        void Timeline_DurationChanged(Timeline sender, float oldDuration, float newDuration)
        {
            if(oldDuration > newDuration)
            {
                // delete keyframes after new duration
                captureAnimation.DeleteAfterTime(newDuration);
            }
            else
            {
                // don't do anything
            }
        }

        #region Initial Tree Construction
        void BuildTree(Animation animation, Transform rootTransform)
        {
            int[] treePath = new int[0];
            animation.RootNode = TreeIter(rootTransform, treePath, 0);
        }

        AnimatedtransformNode TreeIter(Transform transformRef, int[] treePath, int indexAtDepth)
        {
            AnimatedtransformNode newNode = ScriptableObject.CreateInstance<AnimatedtransformNode>();
            newNode.Children = new AnimatedtransformNode[transformRef.childCount];
            newNode.TransformRef = transformRef;

            int[] newPath = new int[treePath.Length + 1];
            for(int i=0; i < treePath.Length; i++)
            {
                newPath[i] = treePath[i];
            }
            newPath[treePath.Length] = indexAtDepth;
            newNode.TreePath = newPath;

            for(int i=0; i < transformRef.childCount; i++)
            {
                newNode.Children[i] = TreeIter(transformRef.GetChild(i), newPath, i);
            }

            return newNode;
        }
        #endregion

        #region Public Editor Methods
		public void SetFileName(string fileName)
		{
			this.fileName = fileName;
		}

        public void StartRecording(bool extendTimeline=false)
        {
            recordCoroutine = StartCoroutine(RecordCoroutine(extendTimeline, 0));
        }

        public void StartWithCountdown(bool extendTimeline = false)
        {
            recordCoroutine = StartCoroutine(RecordCoroutine(extendTimeline, 3));
        }

        public IEnumerator RecordCoroutine(bool extendTimeline, int countdownTime)
        {
            if(countdownTime > 0)
            {
                if(CountdownStart != null)
                {
                    CountdownEvent dispatch = CountdownStart;
                    dispatch(this, countdownTime);
                }

                for(int i=countdownTime; i > 0; i--)
                {
                    yield return new WaitForSeconds(1);

                    if(CountdownTick != null)
                    {
                        CountdownEvent dispatch = CountdownTick;
                        dispatch(this, i);
                    }
                }

                if(CountdownFinished != null)
                {
                    CountdownEvent dispatch = CountdownFinished;
                    dispatch(this, 0);
                }
            }

            isRecording = true;

            if (extendTimeline) timeline.PlayAndExtend();
            else timeline.Play();

            while (true)
            {
                RecordData();

                if ((timeline.PlaybackSpeed == 0) || (!timeline.IsPlaying)) break;

                yield return new WaitForSeconds(keyframeInterval);
            }

            isRecording = false;

            yield break;
        }

        public void ExportAnimationClip()
        {
            string exportFilePath = savePath + fileName;

            AnimationClip clip = new AnimationClip();
            clip.name = fileName;

            if(reportExportTime)
            {
                exportStopwatch.Reset();
                exportStopwatch.Start();
            }

            WriteTransformData(clip, root, captureAnimation.RootNode);

            clip.EnsureQuaternionContinuity();
            AssetDatabase.CreateAsset(clip, exportFilePath);

            if (reportExportTime)
            {
                exportStopwatch.Stop();
                Debug.Log("Exported animation in: " + exportStopwatch.ElapsedMilliseconds + " ms.");
            }
        }

        private void WriteTransformData(AnimationClip clip, Transform root, AnimatedtransformNode transformNode)
        {
            CurveContainer[] curves = transformNode.GenerateCurves();

            for(int i=0; i < curves.Length; i++)
            {
                clip.SetCurve(TransformPathBuilder.GetTransformPathName(root, transformNode.TransformRef), typeof(Transform),
                    curves[i].PropertyName, curves[i].Curve);
            }

            for(int i = 0; i < transformNode.Children.Length; i++)
            {
                WriteTransformData(clip, root, transformNode.Children[i]);
            }
        }
        #endregion

        #region Editor Rendering Methods
        void DrawBoneKeyframe(AnimatedtransformNode node, Matrix4x4 matrix, int keyframe)
        {
            PoseData currentData = node.Keyframes[keyframe];

            //if (currentData.LocalRotation.Equals(zeroRotation)) return;

            for (int i = 0; i < node.Children.Length; i++)
            {
                Matrix4x4 nextMatrix = matrix * Matrix4x4.TRS(currentData.LocalPosition,
                    currentData.LocalRotation, currentData.LocalScale);

                PoseData nextData = node.Children[i].Keyframes[keyframe];

                Gizmos.DrawLine(matrix.MultiplyPoint3x4(currentData.LocalPosition),
                    nextMatrix.MultiplyPoint3x4(nextData.LocalPosition));

                DrawBoneKeyframe(node.Children[i], nextMatrix, keyframe);
            }
        }

        void DrawBoneTime(AnimatedtransformNode node, Matrix4x4 matrix, float time)
        {
            PoseData currentNodeData;
            PoseData currentNodeFuture;

            node.FindPoseDataForTime(time, out currentNodeData, out currentNodeFuture);

            if (!currentNodeData || !currentNodeFuture) return; // in the future, see if we can work with no future node, but a present node

            float currentNodeTValue = Mathf.InverseLerp(currentNodeData.TimeStamp, currentNodeFuture.TimeStamp, time);

            //if (currentNodeData.LocalRotation.Equals(zeroRotation)) return;

            for (int i = 0; i < node.Children.Length; i++)
            {
                Matrix4x4 nextMatrix = matrix * Matrix4x4.TRS(
                    Vector3.Lerp(currentNodeData.LocalPosition, currentNodeFuture.LocalPosition, currentNodeTValue),
                    Quaternion.Slerp(currentNodeData.LocalRotation, currentNodeFuture.LocalRotation, currentNodeTValue),
                    Vector3.Lerp(currentNodeData.LocalScale, currentNodeFuture.LocalScale, currentNodeTValue));

                PoseData nextData;
                PoseData nextDataFuture;

                node.Children[i].FindPoseDataForTime(time, out nextData, out nextDataFuture);
                float nextDataTValue = Mathf.InverseLerp(nextData.TimeStamp, nextDataFuture.TimeStamp, time);

                Gizmos.DrawLine(matrix.MultiplyPoint3x4(Vector3.Lerp(currentNodeData.LocalPosition, currentNodeFuture.LocalPosition, currentNodeTValue)),
                    nextMatrix.MultiplyPoint3x4(Vector3.Lerp(nextData.LocalPosition, nextDataFuture.LocalPosition, currentNodeTValue)));

                DrawBoneTime(node.Children[i], nextMatrix, time);
            }
        }

        void DrawDebugBone(Transform bone)
        {
            for (int i = 0; i < bone.childCount; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(bone.transform.position,
                    bone.GetChild(i).transform.position);
                DrawDebugBone(bone.GetChild(i));
            }
        }

        void OnDrawGizmosSelected()
        {
            if (captureAnimation != null)
            {
                Transform rootParent = root.parent;

                if (!IsRecording)
                {
                    if (!drawDebugKeyframe) DrawBoneTime(captureAnimation.RootNode, (rootParent == null) ? Matrix4x4.identity : rootParent.localToWorldMatrix, timeline.CurrentTime);
                    else DrawBoneKeyframe(captureAnimation.RootNode, (rootParent == null) ? Matrix4x4.identity : rootParent.localToWorldMatrix, drawDebugKeyframeIndex);
                }
            }

            if (root != null && drawDebugBones) DrawDebugBone(root);
        }
        #endregion
    }
}