﻿using UnityEngine;
using System.Collections;

namespace JCV_Animation
{
    public class Animation : ScriptableObject
    {
        [SerializeField] AnimatedtransformNode rootNode;
        public AnimatedtransformNode RootNode { get { return rootNode; } set { rootNode = value; } }

        public void DeleteAfterTime(float time)
        {
            DeleteAfterTime(rootNode, time);
        }

        void DeleteAfterTime(AnimatedtransformNode node, float time)
        {
            int deleteAtIndex = -1;
            for(int i=0; i < node.Keyframes.Length; i++)
            {
                PoseData keyframe = node.Keyframes[i];
                if(keyframe.TimeStamp > time)
                {
                    deleteAtIndex = i;
                    break;
                }
            }

            if(deleteAtIndex >= 0)
            {
                PoseData[] newKeyframeList = new PoseData[deleteAtIndex];

                for(int i=0; i < deleteAtIndex; i++)
                {
                    newKeyframeList[i] = node.Keyframes[i];
                }

                node.Keyframes = newKeyframeList;
            }

            // todo: add some kind of time slicing here?
        }
    }
}