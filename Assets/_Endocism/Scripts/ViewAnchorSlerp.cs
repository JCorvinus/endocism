﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
    public class ViewAnchorSlerp : MonoBehaviour
    {
        [SerializeField] Transform viewTransform;
        [SerializeField] float forwardAmount = 0.5f;

        // Update is called once per frame
        void Update()
        {
            transform.position = Vector3.Slerp(transform.position, viewTransform.position + (viewTransform.forward * 0.3f), forwardAmount);
            transform.LookAt(viewTransform.position, viewTransform.up);
            transform.Rotate(Vector3.up * 180, Space.Self);
        }
    }
}