﻿using UnityEngine;
using System.Collections;

namespace JCV_Animation
{
    public class CurveContainer
    {
        private string propertyName;
        private AnimationCurve curve;

        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        public AnimationCurve Curve
        {
            get { return curve; }
            set { curve = value; }
        }

        public CurveContainer(string propertyName)
        {
            this.propertyName = propertyName;
            curve = new AnimationCurve();
        }

        public void AddValue(float time, float value)
        {
            Keyframe key = new Keyframe(time, value, 0.0f, 0.0f);
            curve.AddKey(key);
        }
    }
}