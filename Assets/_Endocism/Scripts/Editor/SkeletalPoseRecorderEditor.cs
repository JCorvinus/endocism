﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace JCV_Animation.Editor
{
    [CustomEditor(typeof(SkeletalPoseRecorder))]
    public class SkeletalPoseRecorderEditor : UnityEditor.Editor
    {
        SerializedProperty savePath;
        SerializedProperty fileName;

        SerializedProperty pauseOnPoseCapture;
        SerializedProperty reportRecordTime;
        SerializedProperty reportExportTime;

        SerializedProperty drawDebugBones;
        SerializedProperty debugKeyframeIndex;
        SerializedProperty debugKeyframe;

        SerializedProperty root;
        SerializedProperty timeline;

        SkeletalPoseRecorder m_instance;

        bool showAnimationDataFoldout = false;

        void OnEnable()
        {
            m_instance = target as SkeletalPoseRecorder;

            savePath = serializedObject.FindProperty("savePath");
            fileName = serializedObject.FindProperty("fileName");
            drawDebugBones = serializedObject.FindProperty("drawDebugBones");
            pauseOnPoseCapture = serializedObject.FindProperty("pauseOnPoseCapture");
            reportRecordTime = serializedObject.FindProperty("reportRecordTime");
            reportExportTime = serializedObject.FindProperty("reportExportTime");

            debugKeyframeIndex = serializedObject.FindProperty("drawDebugKeyframeIndex");
            debugKeyframe = serializedObject.FindProperty("drawDebugKeyframe");

            root = serializedObject.FindProperty("root");
            timeline = serializedObject.FindProperty("timeline");
        }

        // Use this for initialization
        void Start()
        {
            m_instance = target as SkeletalPoseRecorder;
        }

        void DrawAnimationInfo(Animation animation)
        {
            if(animation != null && animation.RootNode != null) DrawNodeInfo(animation.RootNode);
        }

        void DrawNodeInfo(AnimatedtransformNode node)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(node.TransformRef.name);

            if (node.Keyframes != null && node.Keyframes.Length > 0)
            {
                for (int i = 0; i < node.Keyframes.Length; i++)
                {
                    EditorGUILayout.Vector3Field("Local Position", node.Keyframes[i].LocalPosition);
                    //keyFrameRotation.vector4Value = EditorGUILayout.Vector4Field("Local Rotation", keyFrameRotation.vector4Value);
                    //keyframeScale.vector3Value = EditorGUILayout.Vector3Field("Local Scale", keyframeScale.vector3Value);
                    EditorGUILayout.FloatField("timestamp", node.Keyframes[i].TimeStamp);
                }
            }
            else
            {
                EditorGUILayout.LabelField("No node keyframes");
            }

            if (node.Children != null)
            {
                for (int i = 0; i < node.Children.Length; i++)
                {
                    DrawNodeInfo(node.Children[i]);
                }
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.LabelField("== Path Settings ==");

            if (GUILayout.Button("Set Save Path"))
            {
                string defaultName = serializedObject.targetObject.name + "-Animation.anim";
                string targetPath = EditorUtility.SaveFilePanelInProject("Save Anim File To ..", defaultName, "anim", "please select a folder and enter the file name");

                int lastIndex = targetPath.LastIndexOf("/");
                savePath.stringValue = targetPath.Substring(0, lastIndex + 1);
                string toFileName = targetPath.Substring(lastIndex + 1);

                if (toFileName.IndexOf(".anim") < 0)
                    toFileName += ".anim";

                fileName.stringValue = toFileName;
            }
            EditorGUILayout.PropertyField(savePath);
            EditorGUILayout.PropertyField(fileName);


            EditorGUILayout.Space();

            EditorGUILayout.LabelField("== Recording Controls ==");

            EditorGUILayout.PropertyField(root);
            EditorGUILayout.PropertyField(timeline);

            showAnimationDataFoldout = EditorGUILayout.Foldout(showAnimationDataFoldout, "== Recorded Data ==");

            if(showAnimationDataFoldout)
            {
                DrawAnimationInfo(m_instance.CaptureAnimation);
            }

            EditorGUILayout.LabelField("== settings ==");
            EditorGUILayout.PropertyField(drawDebugBones);
            EditorGUILayout.PropertyField(debugKeyframe);
            EditorGUILayout.PropertyField(debugKeyframeIndex);
            EditorGUILayout.PropertyField(pauseOnPoseCapture);
            EditorGUILayout.PropertyField(reportRecordTime);
            EditorGUILayout.PropertyField(reportExportTime);

            if (Application.isPlaying)
            {
                if(GUILayout.Button("Start Recording"))
                {
                    m_instance.StartRecording();
                }

                if (GUILayout.Button("Export Animation"))
                {
                    m_instance.ExportAnimationClip();
                }                
            }
            else
            {
                EditorGUILayout.LabelField("Start play mode to record.");
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}