﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace JCV_Animation
{
	[CustomEditor(typeof(SkeletalView))]
	public class SkeletalViewEditor : UnityEditor.Editor
	{
		SkeletalView m_instance;

		void Start()
		{
			m_instance = target as SkeletalView;
		}

		private void OnEnable()
		{
			m_instance = target as SkeletalView;
		}

		void OnSceneGUI()
		{
			for(int i=0; i < m_instance.VertCount; i++)
			{
				Vector3 position = m_instance.GetVert(i);
				Handles.Label(position, i.ToString());
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			// draw our buttons here
			if(GUILayout.Button("StartSetVertCoroutine"))
			{
				m_instance.StartSetVertCoroutine();
			}

			if(GUILayout.Button("Advance Bone"))
			{
				m_instance.AdvanceBone();
			}
		}
	}
}