﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

using CatchCo;

namespace JCV_Animation
{
    public class Timeline : MonoBehaviour
    {
        public UnityEvent TimeStarted;
        public UnityEvent TimeStopped;
        public UnityEvent TimeLooped;

        public delegate void DurationChangeHandler(Timeline sender, float oldDuration, float newDuration);
        public DurationChangeHandler TimeDurationChanged;

        [SerializeField] float duration = 1f;
        [SerializeField] float currentTimeIndex = 0;
        float currentTValue = 0;
        [Range(0, 2)]
        [SerializeField] float playbackSpeed = 1;
        [SerializeField] bool loop = false;

        bool isPlaying = false;
        bool extend = false;

        public float CurrentTime { get { return currentTimeIndex; } }
        public float Duration { get { return duration; } }
        public float CurrentTValue { get { return currentTValue; } }
        public float PlaybackSpeed { get { return playbackSpeed; } }
        public bool IsPlaying { get { return isPlaying; } }

        // Update is called once per frame
        void Update()
        {
            if(isPlaying)
            {
                currentTimeIndex += Time.deltaTime * playbackSpeed;
                currentTValue = Mathf.InverseLerp(0, duration, currentTimeIndex);

                if(currentTimeIndex >= duration)
                {
                    if(extend)
                    {
                        duration = currentTimeIndex;
                    }
                    else if(loop)
                    {
                        currentTimeIndex = 0;
                        currentTValue = 0;
                        TimeLooped.Invoke();
                    }
                    else
                    {
                        currentTimeIndex = duration;
                        Pause();
                    }
                }

                if(playbackSpeed == 0)
                {
                    Pause();
                }
            }
        }

        public void SetDuration(float newDuration)
        {
            if (newDuration < 0)
            {
                Debug.LogError("Duration cannot be negative.");
                return;
            }
            
            if (TimeDurationChanged != null)
            {
                DurationChangeHandler dispatch = TimeDurationChanged;
                dispatch(this, duration, newDuration);
            }

            duration = newDuration;
        }

        public void SetTimeTValue(float tValue)
        {
            float time = Mathf.Lerp(0, duration, tValue);
            SetTimeIndex(time);
        }

        public void SetTimeIndex(float timeIndex)
        {
            currentTimeIndex = timeIndex;
            ValidateTimeIndex();
			currentTValue = Mathf.InverseLerp(0, duration, currentTimeIndex);
		}

        public void ValidateTimeIndex()
        {
            currentTimeIndex = Mathf.Max(0, currentTimeIndex);
            currentTimeIndex = Mathf.Min(currentTimeIndex, duration);
        }

        public void OnValidate()
        {
            ValidateTimeIndex();
        }

        [ExposeMethodInEditor]
        public void Play()
        {
            isPlaying = true;
            extend = false;
            TimeStarted.Invoke();
        }

        [ExposeMethodInEditor]
        public void PlayAndExtend()
        {
            extend = true;
            isPlaying = true;
            TimeStarted.Invoke();
        }

        [ExposeMethodInEditor]
        public void Pause()
        {
            isPlaying = false;
            TimeStopped.Invoke();
        }

        [ExposeMethodInEditor]
        public void Stop()
        {
            isPlaying = false;
            currentTimeIndex = 0;
            currentTValue = 0;
            TimeStopped.Invoke();
        }
    }
}