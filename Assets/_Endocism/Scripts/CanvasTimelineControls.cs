﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace JCV_Animation
{
    public class CanvasTimelineControls : MonoBehaviour
    {
        [SerializeField] Timeline timeline;
        [SerializeField] SkeletalPoseRecorder poseRecorder;

        [SerializeField] Button recordButton;
        [SerializeField] Button saveButton;
        [SerializeField] Slider timelineSlider;
        [SerializeField] Text text;

        bool extendTimeline = true;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(timeline.IsPlaying) // if this causes an infinite loop, we need to do some trickery with the slider to differentiate between code 'sets' and UI control 'sets'
            {
                timelineSlider.value = timeline.CurrentTValue;
            }
        }

        public void StartRecording()
        {
            StartCoroutine(Recording());
        }

        IEnumerator Recording()
        {
            text.text = 5.ToString();
            yield return new WaitForSeconds(1);
            text.text = 4.ToString();
            yield return new WaitForSeconds(1);
            text.text = 4.ToString();
            yield return new WaitForSeconds(1);
            text.text = 3.ToString();
            yield return new WaitForSeconds(1);
            text.text = 2.ToString();
            yield return new WaitForSeconds(1);
            text.text = 1.ToString();
            yield return new WaitForSeconds(1);


            text.text = "Recording";
            poseRecorder.StartRecording();

            recordButton.gameObject.SetActive(false);

            while (timeline.IsPlaying) yield return null;

            recordButton.gameObject.SetActive(true);
        }

        public void SetExtendFlag(bool state)
        {
            extendTimeline = state;
        }

        public void UpdateTimeline()
        {
            if (!timeline.IsPlaying)
            {
                timeline.SetTimeIndex(
                    Mathf.InverseLerp(0, timeline.Duration, timelineSlider.value));

                text.text = "idle";
            }
        }
    }
}