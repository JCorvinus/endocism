﻿using UnityEngine;
using System.Collections;

namespace JCV_Animation.ViewControllers
{
    public class SpatialMappingVC : MonoBehaviour
    {
        [SerializeField] GameObject marker;
        [SerializeField] Transform fingertip;

        BoxCollider boxCollider;
        SpatialMapping spatialMapping;

        Vector3 pointA, pointB, // left up, right up
            pointC, pointD; // left down, right down

        Vector3 pointE, pointF, // 3d vert list extension
            pointG, pointH;

        [SerializeField] Vector3 preTransformOutput;

        int dimensionCount;

        void Awake()
        {
            spatialMapping = GetComponent<SpatialMapping>();
            boxCollider = GetComponent<BoxCollider>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (spatialMapping)
            {
                // todo: map fingertip to target

                // set points correctly
                GetBoundaryPoints(spatialMapping.Input.DimensionCount);

                dimensionCount = (spatialMapping) ? spatialMapping.Input.DimensionCount : 2;

                DoMarkerForDimension(dimensionCount);
            }
        }

        void DoMarkerForDimension(int dimension)
        {
            if(dimension == 0)
            {
                preTransformOutput = new Vector3(Mathf.InverseLerp(pointA.x, pointB.x, marker.transform.localPosition.x), 0, 0);
            }
            else if (dimension == 2)
            {
                preTransformOutput = new Vector3(Mathf.InverseLerp(pointA.x, pointB.x, marker.transform.localPosition.x), Mathf.InverseLerp(pointA.y, pointC.y, marker.transform.localPosition.y), preTransformOutput.z);
            }
            else if (dimension == 3)
            {
                throw new System.NotImplementedException();
            }

            Debug.Log("Doing marker mapping");
        }

        void GetBoundaryPoints(int dimensionCount)
        {
            switch (dimensionCount)
            {
                case (1):
                    break;

                case (2):
                    pointA = (Vector3.left * 0.5f * boxCollider.size.x + Vector3.up * 0.5f * boxCollider.size.y);
                    pointB = (Vector3.right * 0.5f * boxCollider.size.x + Vector3.up * 0.5f * boxCollider.size.y);
                    pointC = (Vector3.left * 0.5f * boxCollider.size.x + Vector3.down * 0.5f * boxCollider.size.y);
                    pointD = (Vector3.right * 0.5f * boxCollider.size.x + Vector3.down * 0.5f * boxCollider.size.y);
                    break;

                case (3):
                    break;

                default:
                    break;
            }
        }

        void OnDrawGizmosSelected()
        {
            int dimensionCount = (spatialMapping) ? spatialMapping.Input.DimensionCount : 2;

            if (!Application.isPlaying)
            {
                if(!boxCollider) boxCollider = GetComponent<BoxCollider>();
                GetBoundaryPoints(dimensionCount);
            }

            switch (dimensionCount)
            {
                case (1):
                    break;

                case (2):
                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(transform.TransformPoint(pointA), 0.0015f);
                    Gizmos.DrawSphere(transform.TransformPoint(pointB), 0.0015f);
                    Gizmos.DrawSphere(transform.TransformPoint(pointC), 0.0015f);
                    Gizmos.DrawSphere(transform.TransformPoint(pointD), 0.0015f);

                    break;

                case (3):
                    break;

                default:
                    break;
            }
        }
    }
}