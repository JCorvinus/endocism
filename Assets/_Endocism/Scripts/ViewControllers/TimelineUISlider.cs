﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity.Interaction;

namespace JCV_Animation
{
    public class TimelineUISlider : MonoBehaviour
    {
        private InteractionSlider slider;
        [SerializeField] Timeline timeline;
		[SerializeField] SliderPositionIndicator posIndicator;
		[SerializeField] bool flipValue = false;

        private void Awake()
        {
            slider = GetComponent<InteractionSlider>();
        }

		private void OnEnable()
        {
            slider.HorizontalSlideEvent += (SlideEvent);
        }

        private void OnDisable()
        {
            slider.HorizontalSlideEvent -= (SlideEvent);
        }

        void SlideEvent(float value)
        {
			if (slider.isPressed)
			{
				timeline.SetTimeTValue(flipValue ? 1 - value : value);
			}
        }

		private void Update()
		{
			if(posIndicator != null)
			{
				posIndicator.Override = true;
				posIndicator.OverrideValue = (flipValue) ? 1 - timeline.CurrentTValue : timeline.CurrentTValue;
			}
		}
	}
}