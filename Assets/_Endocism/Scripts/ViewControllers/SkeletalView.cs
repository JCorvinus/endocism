﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
    public class SkeletalView : MonoBehaviour
    {
		enum DrawMode { None, RawPositions, MeshVertices, MeshData, Mesh }

        AnimationZone animZone;
        [SerializeField] Transform uiTransform;
        MeshFilter meshFilter;
        MeshRenderer meshRenderer;
        Canvas canvas;
        TMPro.TMP_Text jointText;

        Mesh mesh;
		[SerializeField]
        List<Vector3> meshVerts;
		[SerializeField]
        int[] faceIDs;

		public int VertCount { get { return meshVerts != null ? meshVerts.Count : 0; } }
		public Vector3 GetVert(int index)
		{
			return meshVerts[index];
		}

		[SerializeField]
        BoneVertIndeces[] boneVertMappings;
        const int vertsPerBone = 6;
        const int faceIndecesPerBone = 8 * 3;

		[SerializeField] DrawMode drawMode = DrawMode.None;
        
		[System.Serializable]
        private struct BoneVertIndeces
        {
            public Transform transform;
			public Transform parent;
            public int vertStartIndex;
            public int faceStartIndex;
        }

		[System.Serializable]
        struct BoneVertPositions
        {
            public Vector3 Begin, End, Zero, One, Two, Three;
        }

		private void Awake()
		{
			animZone = GetComponent<AnimationZone>();
			//poseRecorder = GetComponent<SkeletalPoseRecorder>();
			canvas = uiTransform.GetComponent<Canvas>();
			meshFilter = gameObject.AddComponent<MeshFilter>();
			meshRenderer = gameObject.AddComponent<MeshRenderer>();
			jointText = canvas.GetComponentInChildren<TMPro.TMP_Text>();
		}

		// Use this for initialization
		IEnumerator Start()
        {
            // instantiate the joint text and set the points properly
            int nodeCount = GetNodeCount(animZone.MirrorRoot);

            Debug.Log("node count: " + nodeCount);

			int vertCount = nodeCount * vertsPerBone;
			meshVerts = new List<Vector3>(vertCount);
			for (int i = 0; i < vertCount; i++) meshVerts.Add(Vector3.zero);

            faceIDs = new int[faceIndecesPerBone * nodeCount];
            boneVertMappings = new BoneVertIndeces[nodeCount];

			int[] treePath = new int[0];
			int count = 0; // redundant but we can't avoid that here.

			TraverseNodeRecursive(animZone.MirrorRoot, treePath, 0, ref count);

            mesh = new Mesh();
			mesh.MarkDynamic();
            /*mesh.SetVertices(meshVerts);
            mesh.SetIndices(faceIDs, MeshTopology.Triangles, 0);*/

			yield break;
        }

        int GetNodeCount(Transform rootNode)
        {
            int count = 0;

			CountNodeRecursive(rootNode, ref count);

            return count;
        }

		void CountNodeRecursive(Transform node, ref int count)
		{
			count++;

			for (int i = 0; i < node.childCount; i++) CountNodeRecursive(node.GetChild(i), ref count);
		}

        void TraverseNodeRecursive(Transform node, int[] treePath, int indexAtDepth, ref int count)
        {
            int[] newPath = new int[treePath.Length + 1];
            for (int i = 0; i < treePath.Length; i++)
            {
                newPath[i] = treePath[i];
            }
            newPath[treePath.Length] = indexAtDepth;

			Transform parent = (treePath.Length == 0) ? node : node.parent; // reference ourselves if we are the first node

			BoneVertIndeces boneIndeces = new BoneVertIndeces()
			{
				parent = parent,
				transform = node,
				vertStartIndex = (vertsPerBone * count),
				faceStartIndex = faceIndecesPerBone * count,
			};

            boneVertMappings[count] = boneIndeces;

			count++;

			for (int i = 0; i < node.childCount; i++) TraverseNodeRecursive(node.GetChild(i), treePath, i, ref count);
        }

		[SerializeField]
		bool manualAdvance = false;
		bool advanceBone = false;

		IEnumerator BuildFaces()
		{
			// assemble our face id mappings like so:
			for (int boneIndx = 0; boneIndx < boneVertMappings.Length; boneIndx++)
			{
				BoneVertIndeces boneMapping = boneVertMappings[boneIndx];

				// we have a total of 12 indeces per bone
				// face zero. 0 1 2
				// face one. 3 4 5
				// face two. 6 7 8
				// face three 9 10 11

				int[] boneFaceIDs = new int[faceIndecesPerBone]; // can maybe change this later
				for (int i = 0; i < boneFaceIDs.Length; i++) boneFaceIDs[i] = i;

				// base id
				int baseID = boneIndx * faceIndecesPerBone;

				for (int i = 0; i < boneFaceIDs.Length; i++)
				{
					faceIDs[baseID + i] = boneFaceIDs[i] + boneMapping.vertStartIndex; // we need to figure out how to get the vertex base id in here??
				}

				if (manualAdvance)
				{
					while (!advanceBone) yield return null;
				}
				else
				{
					yield return new WaitForSeconds(0.125f);
				}
			}

			yield break;
		}

		IEnumerator SetVerts()
		{
			// set verts
			for (int boneIndx = 0; boneIndx < boneVertMappings.Length; boneIndx++) // our problem is in this loop somewhere.
			{
				BoneVertIndeces boneVertMapping = boneVertMappings[boneIndx];

				if (manualAdvance)
				{
					while (!advanceBone) yield return null;
				}
				else
				{
					yield return new WaitForSeconds(0.125f);
				}

				advanceBone = false;
				Debug.Log(string.Format("Doing bone {0}", boneIndx));

				BoneVertPositions vertPositions = GetPositionsForBones(boneVertMappings[boneIndx].parent, 
					boneVertMappings[boneIndx].transform);

				int baseIndex = boneVertMapping.vertStartIndex;
				meshVerts[baseIndex] = vertPositions.Begin;
				meshVerts[baseIndex + 1] = vertPositions.Zero;
				meshVerts[baseIndex + 2] = vertPositions.One;
				meshVerts[baseIndex + 3] = vertPositions.Two;
				meshVerts[baseIndex + 4] = vertPositions.Three;
				meshVerts[baseIndex + 5] = vertPositions.End;
			}
		}

		[CatchCo.ExposeMethodInEditor]
		public void StartSetVertCoroutine()
		{
			StartCoroutine(SetVerts());
			StartCoroutine(BuildFaces());
		}

		[CatchCo.ExposeMethodInEditor]
		public void AdvanceBone()
		{
			advanceBone = true;
		}

		// Update is called once per frame
		void Update()
        {
			//SetVerts();
        }

        enum Axis
        {
            xPositive=0,
            xNegative=1,
            yPositive=2,
            yNegative=3,
            zPositive=4,
            zNegative=5
        }

        BoneVertPositions GetPositionsForBones(Transform parent, Transform child)
        {
            Vector3 begin, end, zero, one, two, three;

            begin = parent.transform.position;
            end = child.transform.position;

            float boneSize = 0.0125f;

            Vector3 direction = (end - begin).normalized;

            Axis directionAxis = Axis.xNegative;

            float xDot = Vector3.Dot(parent.right, direction);
            float yDot = Vector3.Dot(parent.up, direction);
            float zDot = Vector3.Dot(parent.forward, direction);

            if (Mathf.Abs(xDot) > Mathf.Abs(yDot) && Mathf.Abs(xDot) > Mathf.Abs(zDot))
            {
                // is biggest
                directionAxis = (xDot > 0) ? Axis.xPositive : Axis.xNegative;
            }
            else if (Mathf.Abs(yDot) > Mathf.Abs(xDot) && Mathf.Abs(yDot) > Mathf.Abs(zDot))
            {
                // is biggest
                directionAxis = (yDot > 0) ? Axis.yPositive : Axis.yNegative;
            }
            else
            {
                // z is biggest
                directionAxis = (zDot > 0) ? Axis.zPositive : Axis.zNegative;
            }

            Vector3 forward = parent.forward, right = parent.right;

            switch (directionAxis)
            {
                case Axis.xPositive:
                    forward = parent.forward;
                    right = parent.up;
                    break;

                case Axis.xNegative:
                    forward = parent.forward;
                    right = parent.up;
                    break;

                case Axis.yPositive:
                    forward = parent.forward;
                    right = parent.right;
                    break;

                case Axis.yNegative:
                    forward = parent.forward;
                    right = parent.right;
                    break;

                case Axis.zPositive:
                    forward = parent.right;
                    right = parent.up;
                    break;

                case Axis.zNegative:
                    forward = parent.right;
                    right = parent.up;
                    break;

                default:
                    break;
            }

            Vector3 squareCenter = Vector3.Lerp(begin, end, 0.2f);
            zero = squareCenter + (forward * boneSize) + (right * -boneSize);
            one = squareCenter + (forward * boneSize) + (right * boneSize);
            two = squareCenter + (forward * -boneSize) + (right * -boneSize);
            three = squareCenter + (forward * -boneSize) + (right * boneSize);

            return new BoneVertPositions { Begin = begin, End = end, Zero = zero, One = one, Two = two, Three = three };
        }

        void DrawChildNode(Transform parent, Transform child)
        {
            BoneVertPositions positions = GetPositionsForBones(parent, child);
            DrawGizmosForVertPositions(positions);
        }

        void DrawGizmosForVertPositions(BoneVertPositions positions)
        {
            // begin to square
            Gizmos.DrawLine(positions.Begin, positions.Zero);
            Gizmos.DrawLine(positions.Begin, positions.One);
            Gizmos.DrawLine(positions.Begin, positions.Two);
            Gizmos.DrawLine(positions.Begin, positions.Three);

            // square
            Gizmos.DrawLine(positions.Zero, positions.One);
            Gizmos.DrawLine(positions.One, positions.Three);
            Gizmos.DrawLine(positions.Three, positions.Two);
            Gizmos.DrawLine(positions.Zero, positions.Two);

            // end to square
            Gizmos.DrawLine(positions.Zero, positions.End);
            Gizmos.DrawLine(positions.One, positions.End);
            Gizmos.DrawLine(positions.Two, positions.End);
            Gizmos.DrawLine(positions.Three, positions.End);
        }

        void DrawStepNodeRecursive(Transform node)
        {
            for (int i = 0; i < node.childCount; i++)
            {
                DrawChildNode(node, node.GetChild(i));
                DrawStepNodeRecursive(node.GetChild(i));
            }            
        }

		void DrawMesh(List<Vector3> verts, int[] triangles)
		{
			int triangleCount = triangles.Length / 3;

			for (int triangleIndex = 0; triangleIndex < triangleCount; triangleIndex++)
			{
				int triangleBaseIndex = triangleIndex * 3;
				int a, b, c;

				a = triangles[triangleBaseIndex];
				b = triangles[triangleBaseIndex + 1];
				c = triangles[triangleBaseIndex + 2];

				bool didFail = false;

				try
				{
					Gizmos.DrawLine(verts[a], verts[b]);
					Gizmos.DrawLine(verts[b], verts[c]);
					Gizmos.DrawLine(verts[c], verts[a]);
				}
				catch (System.IndexOutOfRangeException e)
				{
					Debug.Log("IOOR in triangle at base index: " + triangleBaseIndex);
					didFail = true;
				}
				catch(System.ArgumentOutOfRangeException argE)
				{
					Debug.Log("AOOR in triangle at base index: " + triangleBaseIndex);
					didFail = true;
				}

				if (didFail) break;
			}
		}

		void DrawVertices(List<Vector3> verts)
		{
			if (verts == null) return;

			foreach(Vector3 vertex in verts)
			{
				Gizmos.DrawWireSphere(vertex, 0.01f);
			}
		}

		private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying && animZone == null) animZone = GetComponent<AnimationZone>();
            if ((animZone != null) && (animZone.MirrorRoot != null))
            {
				switch (drawMode)
				{
					case DrawMode.None:
						break;

					case DrawMode.MeshVertices:
						DrawVertices(meshVerts);
						break;

					case DrawMode.RawPositions:
						DrawStepNodeRecursive(animZone.MirrorRoot);
						break;

					case DrawMode.MeshData:
						if (meshVerts != null && faceIDs != null)
						{
							DrawMesh(meshVerts, faceIDs);
						}
						break;
					default:
						break;
				}
            }
        }
    }
}