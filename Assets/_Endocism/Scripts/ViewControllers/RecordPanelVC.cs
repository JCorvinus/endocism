﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity.Interaction;

namespace JCV_Animation.ViewControllers
{
    public class RecordPanelVC : MonoBehaviour
    {
		// controls
        [SerializeField] InteractionButton durationIncrementButton;
        [SerializeField] InteractionButton durationDecrementButton;
        [SerializeField] Timeline timeLine;
        [SerializeField] TMPro.TextMeshProUGUI durationText;
        [SerializeField] TMPro.TextMeshProUGUI timeText;
        [SerializeField] InteractionButton playButton;
        [SerializeField] InteractionButton pausebutton;
        [SerializeField] Transform timeProgressIndicator;
        [SerializeField] InteractionSlider timelineSlider;

        [SerializeField] InteractionButton startRecordingButton;
        [SerializeField] InteractionButton stopRecordingButton;
        [SerializeField] SkeletalPoseRecorder poseRecorder;

		InteractionButton[] buttons;

		// other components
        [SerializeField] Transform userHead;
		[SerializeField] MeshRenderer panelRenderer;
		int diffHash;
		Color panelDefaultColor;
		Color panelRecordingColor = Color.red * 0.75f;
		[SerializeField] AnimationZone animationZone;

		[SerializeField] GameObject grabBar;
		InteractionBehaviour grabBehaviour;

		[Header("Disable while recording")]
        [SerializeField] GameObject[] disableWhileRecording;

		// save menu stuff
		[SerializeField] GameObject attachmentHandContainer; // disable this while save menu is open so that we can't get into weird states.
		Vector3 storedPosition;
		bool isAnimatingChildPanel;
		[SerializeField] SavePanel savePanel;

		private void Awake()
		{
			buttons = GetComponentsInChildren<InteractionButton>(true);
			grabBehaviour = grabBar.GetComponent<InteractionBehaviour>();
		}

		// Use this for initialization
		void Start()
        {
			// visuals
			diffHash = Shader.PropertyToID("_Color");
			panelDefaultColor = panelRenderer.material.GetColor(diffHash);

			// events
            durationIncrementButton.OnPress += (IncrementDuration);
            durationDecrementButton.OnPress += (DecrementDuration);
            playButton.OnPress += (StartPlayBack);
            pausebutton.OnPress += (PausePlayback);
			startRecordingButton.OnPress += (StartRecording);
			stopRecordingButton.OnPress += (StopRecording);
        }

        void IncrementDuration()
        {
            timeLine.SetDuration(timeLine.Duration + 1);
        }

        void DecrementDuration()
        {
            if(timeLine.Duration >= 1) timeLine.SetDuration(timeLine.Duration - 1);
        }

        void StartRecording()
        {
            poseRecorder.StartWithCountdown();
        }

        void StopRecording()
        {
            timeLine.Stop();
		}

        void StartPlayBack()
        {
			Debug.Log("Play started!");
            timeLine.Play();
        }

        void PausePlayback()
        {
            timeLine.Pause();
        }

        public void CallMenu()
        {
            gameObject.SetActive(true);

			grabBar.transform.position = userHead.transform.position + (userHead.transform.forward * 0.33f) + (Vector3.down * 0.2f) /*+ (Vector3.right * 0.4f)*/;
			grabBar.transform.rotation = Quaternion.LookRotation(userHead.transform.forward, Vector3.up);
		}

        public void CallObject()
        {
            animationZone.transform.position = userHead.transform.position + (userHead.transform.forward * 0.3f) + (Vector3.down * 0.2f);
        }

        public void CloseMenu()
        {
            gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            // set our text values
            timeText.text = string.Format("Timeline: {0}", timeLine.CurrentTime);
            durationText.text = timeLine.Duration.ToString();

            // enable or disable buttons depending on if their state is valid
            for(int i=0; i < disableWhileRecording.Length; i++)
            {
                disableWhileRecording[i].SetActive(!poseRecorder.IsRecording);
            }
        }

		public void OpenSaveMenu()
		{
			StartCoroutine(OpenSaveMenuCoroutine());

			DisableControls();

			savePanel.OnCanceled.AddListener(OnSaveCanceled);
			savePanel.OnSaved.AddListener(OnSaved);
		}

		void OnSaved()
		{
			Debug.Log("OnSaved!");
			savePanel.OnSaved.RemoveListener(OnSaved);
			StartCoroutine(CloseSavePanelCoroutine());
		}

		void OnSaveCanceled()
		{
			Debug.Log("OnSaveCanceled");
			savePanel.OnCanceled.RemoveListener(OnSaveCanceled);
			StartCoroutine(CloseSavePanelCoroutine());
		}

		IEnumerator CloseSavePanelCoroutine()
		{
			isAnimatingChildPanel = true;

			yield return new WaitForSeconds(0.25f);

			float time = 0;
			float duration = 0.35f;

			Vector3 goalPosition = storedPosition;

			Vector3 startPostion = grabBar.transform.position;

			while (time < duration)
			{
				float tValue = Mathf.InverseLerp(0, duration, time);

				grabBar.transform.position = Vector3.Lerp(startPostion, goalPosition, tValue);

				time += Time.deltaTime;
				yield return null;
			}

			grabBar.transform.position = goalPosition;

			yield return null;

			isAnimatingChildPanel = false;

			EnableControls();

			yield break;
		}

		void DisableControls()
		{
			foreach (InteractionButton button in buttons) button.controlEnabled = false;
			grabBehaviour.ignoreGrasping = true;
		}

		void EnableControls()
		{
			foreach (InteractionButton button in buttons) button.controlEnabled = true;
			grabBehaviour.ignoreGrasping = false;
		}

		IEnumerator OpenSaveMenuCoroutine()
		{
			isAnimatingChildPanel = true;
			float time = 0;
			float duration = 0.35f;

			// calculate our goal position (for the grab bar, since it moves us)
			Vector3 offset = userHead.transform.forward * 0.35f;
			Vector3 goalPosition = grabBar.transform.position + offset;

			Vector3 startPostion = grabBar.transform.position;
			storedPosition = startPostion;

			while(time < duration)
			{
				float tValue = Mathf.InverseLerp(0, duration, time);

				grabBar.transform.position = Vector3.Lerp(startPostion, goalPosition, tValue);

				time += Time.deltaTime;
				yield return null;
			}

			grabBar.transform.position = goalPosition;

			yield return null;

			savePanel.transform.position = transform.position + transform.forward * -0.1f;
			savePanel.transform.rotation = transform.rotation;
			savePanel.Show();

			isAnimatingChildPanel = false;

			yield break;
		}
    }
}