﻿using System.IO;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using VRKeyboard;

using Leap.Unity.Interaction;

namespace JCV_Animation
{
	public class SavePanel : MonoBehaviour
	{
		public UnityEvent OnSaved;
		public UnityEvent OnCanceled;

		Transform viewCamera;

		[SerializeField] SkeletalPoseRecorder poseRecorder;
		[SerializeField] Keyboard keyboard;
		[SerializeField] string extension;
		[SerializeField] TMPro.TMP_Text fileNamePrefabText;

		[SerializeField] RectTransform scrollRect;
		RectTransform scrollParent;
		[SerializeField] InteractionSlider scrollSlider;
		VerticalLayoutGroup scrollGridGroup;
		float textHeight;
		float scrollHeight;

		private bool isOpen = false;
		public bool IsOpen { get { return isOpen; } }

		private void Awake()
		{
			viewCamera = Camera.main.transform;

			textHeight = scrollRect.GetChild(0).GetComponent<RectTransform>().rect.height;
			scrollGridGroup = scrollRect.GetComponent<VerticalLayoutGroup>();
			scrollParent = scrollRect.transform.parent.GetComponent<RectTransform>();
		}

		private void Start()
		{
			gameObject.SetActive(false);
		}

		int ChildCount()
		{
			int count = 0;
			for (int i = 0; i < scrollRect.childCount; i++) if (scrollRect.GetChild(i).gameObject.activeSelf) count++;
			return count;
		}

		float CalculateContentHeight(int childCount)
		{
			int scrollRectChildCount = childCount;

			return (float)((textHeight * scrollRectChildCount) + (scrollGridGroup.spacing * scrollRectChildCount));
		}

		private void Update()
		{
			if (scrollSlider.isPressed)
			{
				scrollRect.localPosition = Vector3.Lerp(Vector3.zero, Vector3.up * (scrollHeight - (scrollParent.rect.height * 1f)),
					1 - scrollSlider.VerticalSliderPercent);
			}
		}

		public void Show()
		{
			// enable all of our controls

			// get all the files in the specified folder
			// todo: how do we handle a non existent folder? Fallback?
			// we don't have any folder navigation in our browser...
			string directoryPath = Path.Combine(Application.dataPath + "\\..\\", poseRecorder.SavePath);
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			FileInfo[] filesInFolder = directoryInfo.GetFiles(string.Format("*.{0}", extension), SearchOption.TopDirectoryOnly);

			Debug.Log("Scanning folder: " + directoryPath);

			foreach (FileInfo fileInfo in filesInFolder)
			{
				GameObject textCloneGameObject = GameObject.Instantiate(fileNamePrefabText.gameObject, fileNamePrefabText.transform.parent);
				TMPro.TMP_Text textClone = textCloneGameObject.GetComponent<TMPro.TMP_Text>();
				textClone.text = fileInfo.Name;
				textClone.gameObject.SetActive(true);
			}

			scrollHeight = CalculateContentHeight(ChildCount());

			// place our keyboard properly
			keyboard.transform.position = transform.TransformPoint(Vector3.back * 0.3f + Vector3.down * 0.3f);
			keyboard.transform.localScale = Vector3.zero;

			keyboard.LookAtPoint(viewCamera.position - (Vector3.up * 0.2f), transform.up);
			keyboard.Show();

			isOpen = true;

			gameObject.SetActive(true);
		}

		public void TrySave()
		{
			// process our file name
			string fileName = keyboard.GetText();

			if (!fileName.EndsWith(".anim")) fileName = fileName + ".anim";

			poseRecorder.SetFileName(fileName);
			poseRecorder.ExportAnimationClip();

			keyboard.Hide();
			gameObject.SetActive(false);
			OnSaved.Invoke();

			isOpen = false;
		}

		public void Cancel()
		{
			keyboard.Hide();

			gameObject.SetActive(false);
			OnCanceled.Invoke();
			isOpen = false;
		}
	}
}