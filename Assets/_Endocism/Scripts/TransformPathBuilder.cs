﻿using UnityEngine;
using System.Collections;

namespace JCV_Animation
{
    public class TransformPathBuilder
    {
        public static string GetTransformPathName(Transform rootTransform, Transform targetTransform)
        {

            string returnName = targetTransform.name;
            Transform tempObj = targetTransform;

            // it is the root transform
            //if (tempObj == rootTransform)
            //    return tempObj.name;

            while (tempObj != rootTransform)
            {
                returnName = tempObj.parent.name + "/" + returnName;
                tempObj = tempObj.parent;
            }

            return returnName;
        }
    }
}