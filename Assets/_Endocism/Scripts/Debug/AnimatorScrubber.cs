﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
	//[ExecuteInEditMode]
	public class AnimatorScrubber : MonoBehaviour
	{
		[Range(0, 1)]
		[SerializeField]
		float tValue;

		[SerializeField] string stateName;

		Animator animator;

		private void Awake()
		{
			GetComponents();
		}

		void GetComponents()
		{
			if(animator == null) animator = GetComponent<Animator>();
		}

		// Update is called once per frame
		void Update()
		{
			animator.Play(stateName, 0, tValue);
		}
	}
}