﻿using UnityEngine;
using System.Collections;

public class SpatialMapping : MonoBehaviour
{
    [System.Serializable]
    public class MappingTarget
    {
        public string Name;
        public int DimensionCount;
        public float BoundsSize;

        public float[] Data;
    }

    [SerializeField] MappingTarget input;
    [SerializeField] MappingTarget output;

    public MappingTarget Input { get { return input; } }
    public MappingTarget Output { get { return output; } }

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}
}
