﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JCV_Animation
{
    public class CountdownGraphic : MonoBehaviour
    {
        [SerializeField] TMPro.TMP_Text text;
        [SerializeField] SkeletalPoseRecorder recorder;

        // Use this for initialization
        void Start()
        {
            recorder.CountdownStart += Recorder_CountdownStart;
            recorder.CountdownTick += Recorder_CountdownTick;
            recorder.CountdownFinished += Recorder_CountdownFinished;          
        }

        private void Recorder_CountdownFinished(SkeletalPoseRecorder sender, int timeRemaining)
        {
            text.gameObject.SetActive(false);
        }

        private void Recorder_CountdownTick(SkeletalPoseRecorder sender, int timeRemaining)
        {
            text.text = timeRemaining.ToString();

            text.gameObject.SetActive(timeRemaining != 0);
        }

        private void Recorder_CountdownStart(SkeletalPoseRecorder sender, int timeRemaining)
        {
            text.text = timeRemaining.ToString();
            text.gameObject.SetActive(true);
        }
    }
}