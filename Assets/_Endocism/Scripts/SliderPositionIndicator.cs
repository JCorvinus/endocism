﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity.Interaction;

namespace JCV_Animation
{
    public class SliderPositionIndicator : MonoBehaviour
    {
        [SerializeField] InteractionSlider interactionSlider;
		[SerializeField] bool horizValueOverride = false;
		[Range(0,1)]
		[SerializeField] float horizValue = 0;
		float zDepth;

		public bool Override { get { return horizValueOverride = true; } set { horizValueOverride = value; } }
		public float OverrideValue { get { return horizValue; } set { horizValue = value; } }

		private void Awake()
		{
			zDepth = transform.localPosition.z;
		}

		Vector3 GetLeftPoint()
		{
			return interactionSlider.transform.parent.TransformPoint(Vector3.right * -interactionSlider.horizontalSlideLimits.x);
		}

		Vector3 GetRightPoint()
		{
			return interactionSlider.transform.parent.TransformPoint(Vector3.right * interactionSlider.horizontalSlideLimits.x);
		}

		// Update is called once per frame
		void Update()
        {
			Vector3 timeLineLeft = GetLeftPoint();
			Vector3 timelineRight = GetRightPoint();

			float hValue = (horizValueOverride) ? horizValue : interactionSlider.HorizontalSliderPercent;

			if (!horizValueOverride) horizValue = interactionSlider.HorizontalSliderPercent;

			transform.position = Vector3.Lerp(timeLineLeft, timelineRight, 1 - hValue);
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zDepth);
        }

		private void OnDrawGizmos()
		{
			Vector3 timeLineLeft = GetLeftPoint();
			Vector3 timelineRight = GetRightPoint();
			Vector3 currentPos = Vector3.Lerp(timeLineLeft, timelineRight, 1 - interactionSlider.HorizontalSliderPercent);

			Gizmos.matrix = Matrix4x4.TRS(timeLineLeft, transform.rotation, Vector3.one);
			Gizmos.DrawWireCube(Vector3.zero, transform.localScale);
			Gizmos.matrix = Matrix4x4.TRS(timelineRight, transform.rotation, Vector3.one);
			Gizmos.DrawWireCube(Vector3.zero, transform.localScale);

			Gizmos.matrix = Matrix4x4.identity;
		}
	}
}