﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity.Interaction;

namespace JCV_Animation
{
    /// <summary>
    /// A bounding box + interaction engine container for an animatable object/object tree.
    /// </summary>
    public class AnimationZone : MonoBehaviour
    {
        InteractionBehaviour interaction;
        BoxCollider boxCollider;        
        MeshRenderer cubeModel;
        int tintColorHash;
        Color fullTintColor;

        Quaternion zeroRotation = new Quaternion()
        {
            x = 0,
            y = 0,
            z = 0,
            w = 0
        };

        [Tooltip("Root of the preview model.")]
        [SerializeField] Transform mirrorRoot;
        [SerializeField] SkeletalPoseRecorder poseRecorder;
        [SerializeField] Timeline timeline;

        public Transform MirrorRoot { get { return mirrorRoot; } }
        public Vector3 Size { get { return (boxCollider) ? boxCollider.size : Vector3.one * 0.2f; } }
        public Vector3 Center { get {return (boxCollider) ? boxCollider.center : Vector3.zero; } }


        private void Awake()
        {
            interaction = GetComponent<InteractionBehaviour>();
            boxCollider = GetComponentInChildren<BoxCollider>();
            cubeModel = boxCollider.GetComponent<MeshRenderer>();
            tintColorHash = Shader.PropertyToID("_TintColor");
            fullTintColor = Color.white * 0.5f;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            cubeModel.sharedMaterial.SetColor(tintColorHash,
                Color.Lerp(Color.clear, fullTintColor, 1 - interaction.primaryHoverDistance));
        }

        private void LateUpdate()
        {
            // do our pose forcing in lateupdate that way it overwrites anything else
            if(!poseRecorder.IsRecording) PoseMatch();
        }

        void PoseMatch(AnimatedtransformNode node, Transform nodeTransform, float time)
        {
            if (node.Keyframes == null) return;

            if (node.Keyframes.Length > 1)
            {
                PoseData lowPose;
                PoseData highPose;

                node.FindPoseDataForTime(time, out lowPose, out highPose);

                if (lowPose == null) return;
                if (highPose == null)
                {
                    nodeTransform.localPosition = lowPose.LocalPosition;
                    nodeTransform.localRotation = lowPose.LocalRotation;
                    nodeTransform.localScale = lowPose.LocalScale;
                }
                else
                {
                    //if (lowPose.LocalRotation.Equals(zeroRotation)) return; // this was a hold over from some drawing code, maybe to prevent 

                    float tValue = Mathf.InverseLerp(lowPose.TimeStamp, highPose.TimeStamp, time);

                    nodeTransform.localPosition = Vector3.Lerp(lowPose.LocalPosition, highPose.LocalPosition, tValue);

                    //if (lowPose.LocalRotation.Equals(zeroRotation)) return;
                    nodeTransform.localRotation = Quaternion.Slerp(lowPose.LocalRotation, highPose.LocalRotation, tValue);
                    nodeTransform.localScale = Vector3.Lerp(lowPose.LocalScale, highPose.LocalScale, tValue);
                }
            }
            else if (node.Keyframes.Length == 1)
            {
                PoseData pose = node.Keyframes[0];

                nodeTransform.localPosition = pose.LocalPosition;
                nodeTransform.localRotation = pose.LocalRotation;
                nodeTransform.localScale = pose.LocalScale;
            }

            for(int i=0; i < node.Children.Length; i++)
            {
                try
                {
                    PoseMatch(node.Children[i], nodeTransform.GetChild(i), time);
                }
                catch(System.Exception e)
                {
                    // probably index out of range error. Let's see what's up
                    // protip: It's because the orders don't match.
                    // we either need to match by name or figure something out.
                    Debug.LogError("index out of range error. This means your retargeting failed.");
                }
            }
        }

        void PoseMatch()
        {
            if (poseRecorder.CaptureAnimation != null)
            {
                PoseMatch(poseRecorder.CaptureAnimation.RootNode, mirrorRoot, timeline.CurrentTime);
            }
        }
    }
}