﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Leap.Unity;
using Leap.Unity.Interaction;

namespace JCV_Animation
{
	public class MenuSnapOnRelease : MonoBehaviour
	{
		Transform viewCamera;
		InteractionBehaviour interaction;

		float angleThreshold = 15f;

		private void Awake()
		{
			interaction = GetComponent<InteractionBehaviour>();
			viewCamera = Camera.main.transform;
		}

		// Use this for initialization
		void Start()
		{
			interaction.OnGraspedMovement += OnGraspedMovement;
			interaction.OnGraspEnd += () => { InstantAngleSnap(); };
		}

		void OnGraspedMovement(Vector3 preSolvedPos, Quaternion preSolvedRot,
												 Vector3 solvedPos, Quaternion solvedRot,
												 List<InteractionController> graspingControllers)
		{
			SlerpAngleSnap(preSolvedRot);
		}

		[System.Serializable]
		struct AngleSnap
		{
			public Quaternion orientation;
			public float angleDist;
		}

		// up down, left, right angle snaps
		AngleSnap[] angleSnap = new AngleSnap[4];

		void InstantAngleSnap()
		{
			Transform referenceTransform = viewCamera;
			angleSnap[0].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(90, Vector3.forward);
			angleSnap[0].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[0].orientation);

			angleSnap[1].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(270, Vector3.forward);
			angleSnap[1].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[1].orientation);

			angleSnap[2].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(180, Vector3.forward);
			angleSnap[2].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[2].orientation);

			angleSnap[3].orientation = referenceTransform.transform.rotation;
			angleSnap[3].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[3].orientation);

			AngleSnap closestSnap = angleSnap.First(item => item.angleDist == angleSnap.Min(subItem => subItem.angleDist));

			//placementInteraction.rigidbody.MoveRotation(Quaternion.Slerp(placementRigidbody.rotation, closestSnap.orientation, Time.deltaTime * 6f));

			if(closestSnap.angleDist <= angleThreshold) interaction.rigidbody.MoveRotation(closestSnap.orientation);
		}

		void SlerpAngleSnap(Quaternion preSolvedRot)
		{
			Transform referenceTransform = viewCamera;
			angleSnap[0].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(90, Vector3.forward);
			angleSnap[0].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[0].orientation);

			angleSnap[1].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(270, Vector3.forward);
			angleSnap[1].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[1].orientation);

			angleSnap[2].orientation = referenceTransform.transform.rotation * Quaternion.AngleAxis(180, Vector3.forward);
			angleSnap[2].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[2].orientation);

			angleSnap[3].orientation = referenceTransform.transform.rotation;
			angleSnap[3].angleDist = Quaternion.Angle(interaction.rigidbody.rotation, angleSnap[3].orientation);

			AngleSnap closestSnap = angleSnap.First(item => item.angleDist == angleSnap.Min(subItem => subItem.angleDist));

			bool doSnap = (closestSnap.angleDist <= angleThreshold);

			interaction.rigidbody.rotation = (Quaternion.Slerp(interaction.rigidbody.rotation, (doSnap) ? closestSnap.orientation : preSolvedRot, 0.5f));
		}
	}
}