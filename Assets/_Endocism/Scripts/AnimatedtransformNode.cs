﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JCV_Animation
{
    public class AnimatedtransformNode : ScriptableObject
    {
        #region Local Variables
        [SerializeField] AnimatedtransformNode[] children;
        [SerializeField] PoseData[] keyframes;

        Transform transformRef;
        int[] treePath;
        #endregion

        #region Accessors
        public AnimatedtransformNode[] Children
        {
            get { return children; }
            set { children = value; }
        }

        public PoseData[] Keyframes
        {
            get { return keyframes; }
            set { keyframes = value; }
        }

        public int[] TreePath
        {
            get { return treePath; }
            set { treePath = value; }
        }

        public Transform TransformRef
        {
            get { return transformRef; }
            set { transformRef = value; }
        }
        #endregion

        /// <summary>
        /// Adds a keyframe, with real-time performance
        /// assumptions. It assumes the keyframe list is sorted temporally.
        /// </summary>
        /// <param name="pose"></param>
        public void AddKeyframeRealTime(PoseData pose)
        {
            List<PoseData> poses = (keyframes == null) ? new List<PoseData>() : new List<PoseData>(keyframes); // holy shit I am so lazy doing it like this
            int insertIndex = 0;

            // note: replace mode only works on keyframes with exactly the same
            // timestamp. We'll need to find a way to make actual over-write mode a thing
            bool replace = false;
            for(int i=0; i < poses.Count; i++)
            {
                if (keyframes[i].TimeStamp < pose.TimeStamp) insertIndex++;
                else if (keyframes[i].TimeStamp == i)
                {
                    replace = true;
                }
                else break;
            }

            if (replace)
            {
                keyframes[insertIndex] = pose;
            }
            else
            {
                poses.Insert(insertIndex, pose);
                keyframes = poses.ToArray();
            }
        }

        /// <summary>
        /// Given a timestamp, retrieve the keyframes before and after the specified time.
        /// </summary>
        /// <param name="time">Time index to use for search.</param>
        /// <param name="lowKeyframe">The latest keyframe before the time.</param>
        /// <param name="highKeyframe">The earliest keyframe after the time.</param>
        public void FindPoseDataForTime(float time, out PoseData lowKeyframe, out PoseData highKeyframe)
        {
            int lowIndex = -1;
            int highIndex = -1;

            if(keyframes == null || keyframes.Length == 0)
            {
                lowKeyframe = null;
                highKeyframe = null;
                return;
            }

            for (int i = 0; i < keyframes.Length - 1; i++) // this is failing to find something if we're at duration?
            {
                if (time >= keyframes[i].TimeStamp && 
                    time < keyframes[i + 1].TimeStamp)
                {
                    lowIndex = i;
                    highIndex = i + 1;

                    break;
                }
				else if (time >= keyframes[i + 1].TimeStamp)
				{
					lowIndex = i + 1;
					highIndex = i + 1;
				}
            }

            Debug.Assert(lowIndex >= 0, "Low index was below zero, time was: " + time.ToString());
            highIndex = Mathf.Clamp(highIndex, lowIndex, keyframes.Length - 1);

            lowKeyframe = keyframes[lowIndex];
            highKeyframe = keyframes[highIndex];
        }

        public CurveContainer[] GenerateCurves()
        {
            CurveContainer[] curves = new CurveContainer[10];

            curves[0] = new CurveContainer("localPosition.x");
            curves[1] = new CurveContainer("localPosition.y");
            curves[2] = new CurveContainer("localPosition.z");

            curves[3] = new CurveContainer("localRotation.x");
            curves[4] = new CurveContainer("localRotation.y");
            curves[5] = new CurveContainer("localRotation.z");
            curves[6] = new CurveContainer("localRotation.w");

            curves[7] = new CurveContainer("localScale.x");
            curves[8] = new CurveContainer("localScale.y");
            curves[9] = new CurveContainer("localScale.z");

            for(int i=0; i < keyframes.Length; i++)
            {
                PoseData currentKeyframe = keyframes[i];
                curves[0].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalPosition.x);
                curves[1].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalPosition.y);
                curves[2].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalPosition.z);

                curves[3].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalRotation.x);
                curves[4].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalRotation.y);
                curves[5].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalRotation.z);
                curves[6].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalRotation.w);

                curves[7].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalScale.x);
                curves[8].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalScale.y);
                curves[9].AddValue(currentKeyframe.TimeStamp, currentKeyframe.LocalScale.z);
            }

            return curves;
        }
    }
}