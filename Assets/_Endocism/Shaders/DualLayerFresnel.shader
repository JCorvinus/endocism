﻿Shader "Custom/DualLayerFresnel" 
{
	Properties 
	{
		_InnerColor ("Inner Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_InnerPower ("Inner Power", Range(0.5, 8.0)) = 3.0
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	}

	SubShader 
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
       
		Cull Back
		//Blend SrcAlpha OneMinusDstColor
		//BlendOp Sub, Add

		Pass
		{
			ZWrite On
			ColorMask 0
		}
       
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd noshadow alpha:fade // keep this in our back pocket if we can't get blendop to work
       
		struct Input 
		{
			float3 viewDir;
		};
       
		float4 _InnerColor;
		float _InnerPower;
		float4 _RimColor;
		float _RimPower;
       
		void surf (Input IN, inout SurfaceOutput o) 
		{
			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		
			o.Emission = (_InnerColor.rgb * pow (rim, _InnerPower)) + (_RimColor.rgb * pow (rim, _RimPower));
			//o.Albedo = _RimColor * 0.125f;
			o.Alpha = _InnerColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
