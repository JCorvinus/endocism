﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Just testing to see if I can drive values at runtime.
/// </summary>
public class TestAnimatedScript : MonoBehaviour
{
    [SerializeField] private float serializedA;
    public float publicB;
    protected bool protectedBool=false;
    private Vector3 privateVec;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
