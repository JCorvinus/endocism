# ENDOCISM #

Endocism is animation through inhabitation. Use body-tracked controllers and/or VR to animate anything in Unity. Become one with the machine!

Currently, the only working feature is skeletal performance capture. In the future I'll be supporting animating anything through a single workflow.

### What is this repository for? ###

This repo is currently just a backup mirror of what's on my local machine as I work on this. Who knows where it will go from here?

### How do I get set up? ###

First you'll want to make sure you're using Unity 2017.4.1. Endocism should work on newer versions, and will probably be most compatible with 2018.4, but it is untested with any other versions for now. Mostly because the exports are compatible with just about any version of Unity.

Go check out the [wiki](https://bitbucket.org/JCorvinus/endocism/wiki/). All the useful stuff is there.